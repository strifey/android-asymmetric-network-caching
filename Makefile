VERSION=2
PATCHLEVEL=6
SUBLEVEL=29
#EXTRAVERSION=

obj-m += asymcache.o
asymcache-objs := lkm_main.o packet_capture.o cache.o cacheut.o

KDIR=~/aosp/device/tegra

PWD=$(shell pwd)

default:
	make -C $(KDIR) SUBDIRS=$(PWD) modules

clean:
	make -C $(KDIR) SUBDIRS=$(PWD) clean

## Imported from Project 1
#obj-m  := paktcap.o
#paktcap-objs := lkm_main.o packet_capture.o packet_info.o
#KDIR   := ~/android/tegra
#PWD    := $(shell pwd)

#all: $(TGT)
	#$(MAKE) -C $(KDIR) M=$(PWD) modules

#clean:
	#$(MAKE) -C $(KDIR) M=$(PWD) clean 
