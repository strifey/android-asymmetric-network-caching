#undef __KERNEL__
#define __KERNEL__

#undef MODULE
#define MODULE

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/stat.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netdevice.h>
#include <linux/if.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/mm.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <linux/err.h>
#include <crypto/sha.h>
#include "config.h"
#include "packet_capture.h"
#include "cache.h"
#include "cacheut.h"

struct h_header {
	char honoff;
	char hash[160];
};

extern char *it;
extern int d;
extern int hit;
extern Cache* cache;
extern bool timer_trigerred;
/*static char* hashes[50];*/
/*static uint32_t hs = 0;*/


void packetcapture_sethook(struct nf_hook_ops *req, unsigned hooknum)
{
	printk(KERN_INFO "Set hook for %d.\n", hooknum);

	// Set condition for hook
	// hook, pf, hooknum, priority
	req->hook = packet_interceptor_hook;
	req->pf = PF_INET;
	req->hooknum = hooknum;
	req->priority = NF_IP_PRI_FIRST;

	return;
}

unsigned int packet_interceptor_hook(unsigned int hook,
		struct sk_buff *pskb,
		const struct net_device *indev,
		const struct net_device *outdev,
		int (*okfn)(struct sk_buff *)
		)
{
	static int trial = 0;
	// Capturing will be done only in a give time
	if (timer_trigerred != true)
	{
		bool target_if_triggered = false;
		if ((indev != NULL) && 
			!strncmp(indev->name, it, IFNAMSIZ))
		{
			/*printk(KERN_INFO "In : %s", it);*/
			target_if_triggered = true;
		}
		if ((target_if_triggered == true) &&
			(pskb != NULL))
		{
			struct iphdr *ip_header = ip_hdr(pskb);
			if(ip_header->protocol == IPPROTO_TCP){
				printk(KERN_INFO "\n\nHook intercepted TCP!\n");
				chk_cache(pskb);
			}
		}

		printk(KERN_INFO "Hook %2d - NF_IP_\n", trial++);
	}
	return NF_ACCEPT;
}

void chk_cache(struct sk_buff* pskb){
			/*uint8_t i = 0;
			unsigned char out[61];*/
			unsigned char *hash = NULL;
			struct iphdr *ip_header = ip_hdr(pskb);
			unsigned char * temp = (unsigned char*)ip_header;
			int ihl = ip_header->ihl;
			struct tcphdr *tcp_header = (struct tcphdr*)(temp+(ihl*4));
			int doff = tcp_header->doff;
			struct h_header *h = (struct h_header*) (((char*)ip_header)+((doff+ihl)*4));
			printk(KERN_INFO "Src Addr: %08u\n", ntohl(ip_header->saddr));
			printk(KERN_INFO "Total packet len: %u\n", ntohs(ip_header->tot_len));
			printk(KERN_INFO "IP header len: %u\n", ip_header->ihl);
			printk(KERN_INFO "TCP header len: %u\n", tcp_header->doff);
			printk(KERN_INFO "TCP src port: %d\n", ntohs(tcp_header->source));
			/*plaintext_to_sha1(hash, pskb->data, pskb->len);*/
			if(tcp_header->ack && tcp_header->psh && !tcp_header->syn){
				printk(KERN_INFO "Hash on/off: %u", h->honoff);
				if(h->honoff == 50){
					
					uint32_t data_size;
					char*new_e, *dp = NULL;
					int d_offset = ((ihl+doff)*4)+sizeof(struct h_header);

					printk(KERN_INFO "Not in hash table. Adding...\n");				
					hash = kmalloc(sizeof(char)*160, GFP_KERNEL);
					memcpy(hash, h->hash, 160);

					data_size = (ntohs(ip_header->tot_len))- d_offset;
					new_e = kmalloc(sizeof(char)*data_size, GFP_KERNEL);
					dp = ((char*)ip_header)+d_offset;
					memcpy(new_e, dp, data_size);

					add(hash, new_e);
				}
				else if(h->honoff == 49){
					
					CacheEntry *e = NULL;
					
					hash = kmalloc(sizeof(char)*160, GFP_KERNEL);
					memcpy(hash, h->hash, 160);
					e =  get(hash);
					kfree(hash);
					if(e){
						hit++;
						printk(KERN_INFO "Found in table:\n");
						printk(KERN_INFO "%s\n", e->data);
					}
					else{
						printk(KERN_INFO "ERROR Supposed to be in table, but couldn't find\n");
						}
					}
				else{
					printk(KERN_INFO "honoff value is not 1 or 2\n");
					}
				}
			else{
				printk(KERN_INFO "Either a SYN packet or not a PSH packet. Ignoring\n");
			}
			/*if(hashes[hs] == NULL)*/
				/*hashes[hs] = hash;*/
			/*else if(*hashes[hs] == *hash)*/
					/*printk(KERN_INFO "Found the packet we got before!\n");*/
			/*for(i = 0; i< hs; i++){*/
				/*if(*hash == *hashes[i])*/
					/*break;*/
			/*}*/
			/*if(i == hs){*/
				/*if(hs < 50){*/
					/*hashes[hs] = hash;*/
					/*hs++;*/
					/*printk(KERN_INFO "Adding hash to table\n");*/
				/*}*/
				/*else*/
					/*printk(KERN_INFO "Not in list, but list is full\n");*/
			/*}*/
			/*else{*/
				/*printk(KERN_INFO "Found hash in table\n");*/
			/*}*/
}


int plaintext_to_sha1(unsigned char *hash, const char *plaintext, 
		unsigned int len) {
	struct crypto_hash *tfm;
	struct scatterlist sg;
	struct hash_desc desc;
	/*char hash[SHA1_DIGEST_LENGTH];*/
	if (len > PAGE_SIZE) {
		printk(KERN_ERR "Plaintext password too large (%d "
				"characters).  Largest possible is %lu "
				"bytes.\n", len, PAGE_SIZE);
		return -EINVAL;
	}
	tfm = crypto_alloc_hash("sha1", 0, CRYPTO_ALG_ASYNC);
	if (tfm == NULL) {
		printk(KERN_ERR "Failed to load transform for SHA1\n");
		return -EINVAL;
	}
	desc.tfm = tfm;
	desc.flags = 0;

	sg_init_one(&sg, (u8 *)plaintext, len);
	crypto_hash_init(&desc);
	crypto_hash_update(&desc, &sg, len);
	crypto_hash_final(&desc, hash);
	crypto_free_hash(tfm);
	return 0;
}
