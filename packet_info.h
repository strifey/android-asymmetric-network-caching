

typedef struct _dst_info {
	unsigned daddr;
	unsigned count;
	struct _dst_info *pst_next_dst;
} dst_info_t;

typedef struct _src_info {
	unsigned saddr;
	unsigned src_dst_count;
 	struct _dst_info *pst_first_dst;
} src_info_t;	

typedef struct _packet_capture_info {
	dst_info_t dst_pool[DST_INFO_POOL_SIZE];
	src_info_t src_pool[SRC_INFO_POOL_SIZE];
	unsigned dst_count;
	unsigned next_src_idx;
} packet_capture_info_t;

unsigned int packet_interceptor_hook(unsigned int hook,
		struct sk_buff *pskb,
		const struct net_device *indev,
		const struct net_device *outdev,
		int (*okfn)(struct sk_buff *)
		);

void init_packet_capture_info(void);
void put_packet_info(unsigned saddr, unsigned daddr);
void print_packet_info(void);

