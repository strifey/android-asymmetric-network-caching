#undef __KERNEL__
#define __KERNEL__

#undef MODULE
#define MODULE

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/stat.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netdevice.h>
#include <linux/if.h>
#include <linux/ip.h>

#include <linux/mm.h>
#include <linux/crypto.h>
#include <linux/scatterlist.h>
#include <linux/err.h>
#include <crypto/sha.h>

#include "config.h"
#include "packet_capture.h"
#include "packet_info.h"
#include "cache.h"
#include "cacheut.h"

char *it= NULL;
int d;
int hit =0;
packet_capture_info_t capture_info;

// From netfilter_ipv4.h 
// todo: Need to fix the problems, why !__KERNELL__ ?
/* IP Hooks */
/* After promisc drops, checksum checks. */
#define NF_IP_PRE_ROUTING       0
/* If the packet is destined for this box. */
#define NF_IP_LOCAL_IN          1
/* If the packet is destined for another interface. */
#define NF_IP_FORWARD           2
/* Packets coming from a local process. */
#define NF_IP_LOCAL_OUT         3
/* Packets about to hit the wire. */
#define NF_IP_POST_ROUTING      4
#define NF_IP_NUMHOOKS          5


module_param(d, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(d, "Hooking Time in Sec");
module_param(it, charp, 0);
MODULE_PARM_DESC(it, "Interface");

MODULE_LICENSE("GPL");

static struct nf_hook_ops nfho_in/*, nfho_forward, nfho_out*/;
struct timer_list capture_timer;
bool timer_trigerred = false;

void set_timer_expired(unsigned long int data)
{
	timer_trigerred = true;	
	printk(KERN_INFO "Timer Triggered.\n");
}

static int __init packetcapture_init(void)
{
	printk(KERN_INFO "Init netfilter module.\n");
	printk(KERN_INFO "Interface: %s\n", it);
	printk(KERN_INFO "Duration: %3d sec.\n", d);
	initCache();

	packetcapture_sethook(&nfho_in, NF_IP_LOCAL_IN);
	printk(KERN_INFO "Register cache hook\n");
	nf_register_hook(&nfho_in);
	setup_timer(&capture_timer, set_timer_expired, 0);
	mod_timer(&capture_timer, jiffies + msecs_to_jiffies(d* 1000));
	//Doing cache testing
	// testAdding();
	return 0;
}

static void __exit packetcapture_cleanup(void)
{
	del_timer(&capture_timer);

	nf_unregister_hook(&nfho_in);
	printCache();
	printk(KERN_INFO "Hits :%d\n", hit);

	printk(KERN_INFO "Cleanup netfilter module.\n");
}

module_init(packetcapture_init);
module_exit(packetcapture_cleanup);

