#include "cache.h"

//Regular hashtable tests
void testAdding(void);
void testResizing(void);
void testRemoving(void);
void testGetting(void);

//Linked List hashtable tests
void testAddingLL(void);
void testResizingLL(void);
void testRemovingLL(void);
void testGettingLL(void);