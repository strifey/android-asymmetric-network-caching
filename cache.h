#ifndef CACHE_H
#define CACHE_H
typedef struct _CacheEntry{
	char* data;
	char* hash;
	struct _CacheEntry* next;

} CacheEntry;


typedef struct _Cache{
	unsigned int entries;
	unsigned int size;
	unsigned int loadFactor;
	CacheEntry** table;
} Cache;


//Initializes the cache
void initCache(void);
//Initializes the cache with a specific starting size
void initCacheSize(unsigned int size);
//Creates a hash from the given data
unsigned int createHash(char* hash);
//Creates a CacheEntry based on the given data
CacheEntry* createCacheEntry(char* hash, char* data);
//Deletes a CacheEntry and its data associated data inside
void deleteCacheEntry(CacheEntry* entry);
//Adds the given data to the cache, automatically creating the CacheEntry
void add(char* hash, char* data);
//Adds the cacheentry to the cache
void addEntry(CacheEntry* newEntry);
//Resizes the cache, used when loadfactor is too high
void resize(void);
//Deletes the given data, if it exists, from the cache
void delete(char* hash, char* data);
//Deletes the specific cacheentry from the cache,
//Based on the contents and not the CacheEntry pointer itself
void deleteEntry(CacheEntry* entry);
//Returns a cacheentry from the given data
CacheEntry* get(char* hash);
//Prints the cache
void printCache(void);
//Deletes the cache and all of it's entries + their data
void deleteCache(void);
#endif
