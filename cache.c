#include "cache.h"
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/module.h>
 #include <linux/slab.h>
// #include <linux/moduleparam.h>
// #include <linux/kernel.h>
// #include <linux/vmalloc.h>
// #include <linux/kernel.h>
 #include <linux/mm.h>
// #include <linux/init.h>
// #include <linux/if.h>
// #include <linux/ip.h>

Cache* cache;

//Default constants
static unsigned int defaultSize = 11;
static unsigned int defaultLoadFactor = 6;
static unsigned int sha1Length = 160;
unsigned char out[61];

//Initializes the cache with the default size
void initCache(void){
	initCacheSize(defaultSize);
}
//Initializes the cache with the inputted size
void initCacheSize(unsigned int size){
	cache = kzalloc(sizeof(cache), GFP_KERNEL);
	cache -> size = size;
	cache -> entries = 0;
	cache -> loadFactor = defaultLoadFactor;
	cache -> table = kzalloc(sizeof(CacheEntry*) * size, GFP_KERNEL);
}

//Creates a hashValue value
//Currently only uses the char* hash
unsigned int createHash(char* hash){
	//Make sure to change this hashValue for the kernel!
	unsigned int index = 0;
	unsigned int hashValue = 0;
	// unsigned int length = strlen(hash);
	unsigned int length = sha1Length;
	for(index = 0; index < length; index++){
		hashValue += hash[index];
	}

	hashValue = hashValue % (cache -> size);
	/*printk(KERN_INFO "hashValue is %d for hash %s\n", hashValue, hash);*/
	return hashValue;
}

//Creates a CacheEntry* 
CacheEntry* createCacheEntry(char* hash, char* data){
	CacheEntry* entry = kzalloc(sizeof(CacheEntry), GFP_KERNEL);
	entry -> data = data;
	entry -> hash = hash;
	entry -> next = NULL;
	return entry;
}

//Deletes a CacheEntry* and all of its data
void deleteCacheEntry(CacheEntry* entry){
	//Don't needs this as the hash is already malloc'd
	// if(entry -> hash != NULL){
	// 	printk(KERN_INFO "kfreeing hash: %s\n", entry -> hash);
	// 	kfree(entry -> hash);
	// }

	if(entry -> data != NULL){
		// printk(KERN_INFO "kfreeing data\n");
		kfree(entry -> data);
	}

	// printk(KERN_INFO "kfreeing entry\n");
	kfree(entry);
}

//Adds a new entry to the table
void add(char* hash, char* data){
	CacheEntry* newEntry = createCacheEntry(hash, data);
	addEntry(newEntry);
}

//Adds an entry to the table using a CacheEntry*
void addEntry(CacheEntry* newEntry){
	unsigned int currentLoadFactor;
	unsigned int hashValue;
	CacheEntry** table;
	CacheEntry* currentEntry;
	if(newEntry -> next != NULL)
		newEntry -> next = NULL;
	currentLoadFactor = cache -> entries;
	//printk(KERN_INFO "currentLoadFactor is: %f\n", currentLoadFactor);
	if(currentLoadFactor > cache -> loadFactor){
		resize();
	}
	hashValue = createHash(newEntry -> hash);
	// newEntry -> hashValue = hashValue;
	table = cache -> table;
	currentEntry = table[hashValue];
	if(currentEntry == NULL){
		table[hashValue] = newEntry;
	}
	//Checking the external chaining linked list
	else{
		while(currentEntry -> next != NULL){
			currentEntry = currentEntry -> next;
		}
		currentEntry -> next = newEntry;
	}
	cache -> entries++;
}

//Resizes the hashtable if the loadfactor is too large
void resize(void){
	unsigned int newSize = (cache -> size) * 2 + 1; 
	CacheEntry** oldTable = cache -> table;
	CacheEntry** newTable = kzalloc(sizeof(CacheEntry*) * newSize, GFP_KERNEL);
	unsigned int oldSize = cache -> size;
	unsigned int index;
	CacheEntry* nextEntry;
	CacheEntry* entry;
	// printk(KERN_INFO "resize()\n");
	cache -> loadFactor = cache -> loadFactor * 2 + 1;
	cache -> table = newTable;
	cache -> size = newSize;
	cache -> entries = 0;
	for(index = 0; index < oldSize; index++){
		CacheEntry* entry = oldTable[index];
		if(entry != NULL){
			CacheEntry* nextEntry = entry -> next;
			addEntry(entry);
			// printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);
			entry = nextEntry;
			while(entry != NULL){
				// printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);
				// CacheEntry* nextEntry = entry -> next;
				// addEntry(nextEntry);
				// entry -> next = nextEntry -> next;

				// addEntry(entry);
				// entry = entry -> next;
				nextEntry = entry -> next;
				addEntry(entry);
				entry = nextEntry;
			}
		}
	}
	kfree(oldTable);

}

//Deletes an entry from the table
void delete(char* hash, char* data){
	unsigned int hashValue = createHash(hash);
	CacheEntry** table = cache -> table;
	CacheEntry* entry = table[hashValue];
	CacheEntry* toRemove;
	CacheEntry* probe;
	if(entry != NULL){
		// printk(KERN_INFO "Entry is not NULL\n");
		if(entry -> next == NULL){
			// printk(KERN_INFO "Entry's next is NULL\n");
			if(memcmp(hash, entry -> hash, sha1Length) == 0){
				// printk(KERN_INFO "Entry's hash is the same as inputted hash\n");
				table[hashValue] = NULL;
			}
			else{
				// printk(KERN_INFO "Entry's hash is different from inputted hash\n");
				entry = NULL;
			}
		}
		//Make sure to check for something other than memcmp when in kernel!
		else{
			//Linked list remove
			//Removing head
			if(memcmp(hash, entry -> hash, sha1Length) == 0){
				table[hashValue] = entry -> next;
			}
			else{
				//Remove head's next
				if(memcmp(hash, entry -> next -> hash, sha1Length) == 0){
					toRemove = entry -> next;
					entry -> next = toRemove -> next;
					entry = toRemove;
				}
				else{
					probe = entry -> next;
					entry = NULL;
					while(probe -> next != NULL){
						if(memcmp(hash, probe -> next -> hash, sha1Length) == 0){
							entry = probe -> next;
							probe -> next = entry -> next;
							break;
						}
						probe = probe -> next;
					}
				}
			}
		}

		//Delete the entry at the end
		if(entry != NULL){
			// printk(KERN_INFO "deleteCacheEntry()\n");
			deleteCacheEntry(entry);
			cache -> entries--;
		}
	}
	
}
void deleteEntry(CacheEntry* entry){
	delete(entry -> hash, entry -> data);
}

/*****
	MAKE SURE TO USE MEMCMP INSTEAD OF memcmp!!!!!!!!!!!!!!!!!!!!!!!
	********************
	*********************
	*****************
	******************
*****/

CacheEntry* get(char* hash){
	unsigned int hashValue;
	CacheEntry** table;
	CacheEntry* entry;
	CacheEntry* probe;
	
	// printk(KERN_INFO "in get()\n");
	if(hash == NULL)
		return NULL;

	hashValue = createHash(hash);
	table = cache -> table;
	entry = table[hashValue];

	if(entry != NULL && memcmp(hash, entry -> hash, sha1Length) != 0){
		if(entry -> next == NULL){
			entry = NULL;
		}
		else{
			probe = entry -> next;
			entry = NULL;
			while(probe != NULL){
				if(probe -> hash != NULL &&
					memcmp(hash, probe -> hash, sha1Length) == 0){
					entry = probe;
					break;
				}
				probe = probe -> next;
			}
		}
	}

	return entry;
}


void printCache(void){
	unsigned int index = 0;
	CacheEntry** table = cache -> table;
	printk(KERN_INFO "Printing out cache!\n");
	for(index = 0; index < cache -> size; index++){
		CacheEntry* entry = table[index];
		if(entry == NULL){
			/*printk(KERN_INFO "CacheEntry at index %d is NULL\n", index);*/
		}
		else{
			printk(KERN_INFO "CacheEntry at index %d has a hash of:\n %s\n", index, entry->hash);
			entry = entry -> next;
			while(entry != NULL){
				printk(KERN_INFO "\tNext node also has a hash of: %s\n", entry -> hash);
				entry = entry -> next;
			}
		}
	}
	printk(KERN_INFO "Current entries: %d\n", cache -> entries);
	printk(KERN_INFO "Current size: %d\n", cache -> size);
	printk(KERN_INFO "Current loadFactor: %d\n", cache -> loadFactor);
}

void deleteCache(void){
	unsigned int index = 0;
	CacheEntry** table = cache -> table;
	CacheEntry* entry;
	CacheEntry* probe;
	// printk(KERN_INFO "Deleting cache!\n");
	for(index = 0; index < cache -> size; index++){
		entry = table[index];
		if(entry == NULL){
			continue;
		}
		if(entry -> next == NULL){
			deleteCacheEntry(entry);
		}
		else{
			// probe = entry;
			// entry = entry -> next;
			// deleteCacheEntry(probe);
			while(entry != NULL){
				probe = entry;
				entry = entry -> next;
				deleteCacheEntry(probe);
			}
		}
	}

	// printk(KERN_INFO "Freeing cache!\n");
	kfree(cache);
	cache = NULL;
}
