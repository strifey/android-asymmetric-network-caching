#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/netdevice.h>
#include <linux/if.h>
#include <linux/ip.h>

#include "config.h"
#include "packet_capture.h"
#include "packet_info.h"

extern packet_capture_info_t capture_info;

static packet_capture_info_t *sst_capture_info = &capture_info;

static dst_info_t * _get_next_dst_info(void);
static src_info_t * _get_src_info_index(unsigned saddr);
static dst_info_t * _get_find_dst_info(src_info_t *pst_src_info, unsigned daddr);

void init_packet_capture_info(void)
{
	int idx;
	src_info_t *pst_src = NULL;
	dst_info_t *pst_dst = NULL;

	sst_capture_info->next_src_idx = 0;
	sst_capture_info->dst_count = 0;

	for (idx = 0; idx < SRC_INFO_POOL_SIZE; idx++)
	{
		pst_src = &(sst_capture_info->src_pool[idx]);
		pst_src->saddr = 0;
		pst_src->src_dst_count = 0;
		pst_src->pst_first_dst = NULL;

	}

	for (idx = 0; idx < DST_INFO_POOL_SIZE; idx++)
	{
		pst_dst = &(sst_capture_info->dst_pool[idx]);
		pst_dst->daddr = 0;
		pst_dst->count = 0;
		pst_dst->pst_next_dst = NULL;
	}

}

void put_packet_info(unsigned saddr, unsigned daddr)
{
	src_info_t *pst_cur_src_info = NULL;
	dst_info_t *pst_cur_dst_info = NULL;
	
	pst_cur_src_info = _get_src_info_index(saddr); 

	printk(KERN_INFO "Try to put saddr[%8u] & daddr[%8u]\n", saddr, daddr);

	if (pst_cur_src_info != NULL)
	{
		pst_cur_dst_info = _get_find_dst_info(pst_cur_src_info, daddr);

		if (pst_cur_dst_info != NULL)
		{
			pst_cur_dst_info->count++;
			printk(KERN_INFO "Put saddr[%8u] & daddr[%8u] successfully incremented\n", saddr, daddr);
			printk(KERN_INFO "DST Count %d\n", pst_cur_dst_info->count);
		}

	}

	return;
}

void print_packet_info(void)
{
	unsigned src_idx;

	printk(KERN_INFO "=============================\n");
	printk(KERN_INFO "= roject 2 Packet Capturing. \n");
	printk(KERN_INFO "=============================\n");
	printk(KERN_INFO "Sources number : %d\n", sst_capture_info->next_src_idx - 1);

	for (src_idx = 0; src_idx < sst_capture_info->next_src_idx; src_idx++)
	{
		src_info_t *pst_cur_src_info = &(sst_capture_info->src_pool[src_idx]);
		dst_info_t *pst_cur_dst_info = pst_cur_src_info->pst_first_dst;
		printk(KERN_INFO "---------------------------------------------\n");
		printk(KERN_INFO "Src Addr : %8u\n", pst_cur_src_info->saddr);
		printk(KERN_INFO "Dest Count : %u\n", pst_cur_src_info->src_dst_count);
		while (pst_cur_dst_info != NULL)
		{
			printk(KERN_INFO "Dst Addr [Count] : %8u [%4d]", pst_cur_dst_info->daddr, pst_cur_dst_info->count);	
			pst_cur_dst_info = pst_cur_dst_info->pst_next_dst;
		}

	}
	printk(KERN_INFO "===========================================\n");
}

static dst_info_t * _get_find_dst_info(src_info_t *pst_src_info, unsigned daddr)
{
	unsigned dst_idx;
	dst_info_t *pst_dst_info = pst_src_info->pst_first_dst;
	dst_info_t *pst_prev_dst_info = NULL;

	if (pst_dst_info != NULL)
	{
		printk (KERN_INFO "Src Dst Count : %d\n", pst_src_info->src_dst_count);
		for (dst_idx = 0; dst_idx < pst_src_info->src_dst_count; dst_idx++)
		{
			pst_prev_dst_info = pst_dst_info;
			if (pst_dst_info->daddr == daddr)
			{
				printk(KERN_INFO "Found DstInfo Idx for : %8u\n", daddr);
				break;
			}

			pst_prev_dst_info = pst_dst_info;
			pst_dst_info = pst_dst_info->pst_next_dst;

		} //while (pst_dst_info != NULL);
	}

	if (pst_dst_info == NULL)
	{
		dst_info_t *pst_new_dst_info = _get_next_dst_info();	
		printk(KERN_INFO "Getting new DstInfo Idx for : %8u\n", daddr);
		if (pst_new_dst_info != NULL)
		{
			printk(KERN_INFO "Add new Dst Info\n");
			pst_new_dst_info->count = 0;
			pst_new_dst_info->daddr = daddr;
			pst_new_dst_info->pst_next_dst = NULL;
			pst_dst_info = pst_new_dst_info;
			if (pst_prev_dst_info == NULL)
			{
				pst_src_info->pst_first_dst = pst_dst_info;
			}
			else
			{
				pst_prev_dst_info->pst_next_dst = pst_dst_info;
			}
			pst_src_info->src_dst_count++;
		}
	}

	return pst_dst_info;
}

static dst_info_t * _get_next_dst_info(void)
{
	dst_info_t *pst_next_dst_info = NULL;

	if (sst_capture_info->dst_count < DST_INFO_POOL_SIZE-1)
	{
		printk(KERN_INFO "Next DstInfo Idx : %3d\n", sst_capture_info->dst_count);
		pst_next_dst_info = &(sst_capture_info->dst_pool[sst_capture_info->dst_count++]);
	}
	else
	{
		printk(KERN_INFO "============ No More Keep ==============\n");
		printk(KERN_INFO "Next DstInfo Not Exist\n");
	}

	return pst_next_dst_info;
}


static src_info_t * _get_src_info_index(unsigned saddr)
{
	src_info_t * pst_src = NULL;
	unsigned src_idx;

	for (src_idx = 0; src_idx < sst_capture_info->next_src_idx; src_idx++)
	{
		pst_src = &(sst_capture_info->src_pool[src_idx]);
		if (pst_src -> saddr == saddr)
		{
			printk(KERN_INFO "Src Addr Already Exists.: %8u\n", pst_src->saddr);
			return pst_src;
		}
		pst_src = NULL;
	}

	if (pst_src == NULL)
	{
		if (sst_capture_info->next_src_idx < SRC_INFO_POOL_SIZE)
		{
			pst_src = &(sst_capture_info->src_pool[sst_capture_info->next_src_idx++]);
			pst_src->saddr = saddr;
			pst_src->pst_first_dst = NULL;
			printk(KERN_INFO "Src Addr is newly allocated.: %8u\n", pst_src->saddr);
			printk(KERN_INFO "Next Source Index will be : %d", sst_capture_info->next_src_idx);
		}
	}

	return pst_src;
}
