#define SHA1_DIGEST_LENGTH 20
#define IPPROTO_AC (146)

void packetcapture_sethook(struct nf_hook_ops *req, unsigned hooknum);

int plaintext_to_sha1(unsigned char *hash, const char *plaintext
		, unsigned int len);
unsigned int packet_interceptor_hook(unsigned int hook,
		struct sk_buff *pskb,
		const struct net_device *indev,
		const struct net_device *outdev,
		int (*okfn)(struct sk_buff *)
		);

void chk_cache(struct sk_buff* pskb);
