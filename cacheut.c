#include "cacheut.h"
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/vmalloc.h>
extern Cache* cache;
void testAdding(void){
	printk(KERN_INFO "*****testAdding()!\n");
	initCache();
	add("aaaaaaaaaaaaaaaaaaaa", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("cccccccccccccccccccc", NULL);
	add("dddddddddddddddddddd", NULL);
	printCache();
	// printk(KERN_INFO "Deleting Cache!\n");
	deleteCache();
	if(cache == NULL){
		printk(KERN_INFO "Cache is now null and freed!\n");
	}
	else{
		printk(KERN_INFO "Cache is not free!\n");
	}
}

void testResizing(void){
	printk(KERN_INFO "*****testResizing()!\n");
	initCache();
	printk(KERN_INFO "Cache size is now: %d!\n", cache -> size);
	printk(KERN_INFO "Cache loadfactor is now: %d!\n", cache -> loadFactor);
	add("aaaaaaaaaaaaaaaaaaaa", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("cccccccccccccccccccc", NULL);
	add("dddddddddddddddddddd", NULL);
	add("eeeeeeeeeeeeeeeeeeee", NULL);
	add("ffffffffffffffffffff", NULL);
	add("gggggggggggggggggggg", NULL);
	add("hhhhhhhhhhhhhhhhhhhh", NULL);
	add("iiiiiiiiiiiiiiiiiiii", NULL);
	add("jjjjjjjjjjjjjjjjjjjj", NULL);
	printCache();
	printk(KERN_INFO "Cache size is now: %d!\n", cache -> size);
	printk(KERN_INFO "Cache loadfactor is now: %d!\n", cache -> loadFactor);
	deleteCache();
}

void testRemoving(void){
	printk(KERN_INFO "*****testRemoving()!\n");
	initCache();

	add("aaaaaaaaaaaaaaaaaaaa", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("cccccccccccccccccccc", NULL);
	add("dddddddddddddddddddd", NULL);
	add("eeeeeeeeeeeeeeeeeeee", NULL);
	printCache();

	delete("aaaaaaaaaaaaaaaaaaaa", NULL);
	delete("bbbbbbbbbbbbbbbbbbbb", NULL);
	delete("cccccccccccccccccccc", NULL);
	printk(KERN_INFO "Removing a, c, e\n");
	printCache();
	deleteCache();
}

void testGetting(void){
	CacheEntry* entry;
	printk(KERN_INFO "*****testGetting()!\n");
	initCache();
	add("aaaaaaaaaaaaaaaaaaaa", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("cccccccccccccccccccc", NULL);
	add("dddddddddddddddddddd", NULL);
	add("eeeeeeeeeeeeeeeeeeee", NULL);
	printCache();

	entry = get("aaaaaaaaaaaaaaaaaaaa");
	printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);
	entry = get("cccccccccccccccccccc");
	printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);
	entry = get("eeeeeeeeeeeeeeeeeeee");
	printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);
	entry = get(NULL);
	if(entry == NULL){
		printk(KERN_INFO "Entry is NULL\n");
	}
	entry = get("a");
	if(entry == NULL){
		printk(KERN_INFO "Entry is NULL\n");
	}
	deleteCache();
	printk(KERN_INFO "\n");
}

void testAddingLL(void){
	printk(KERN_INFO "*****testAddingLL()!\n");
	initCache();
	add("aaaaaaaaaaaaaaaaaaae", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("aaaaaaaaaaaaaaaaaabd", NULL);
	add("aaaaaaaaaaaaaaaaaacc", NULL);
	// add("c", NULL);
	// add("d", NULL);
	printCache();
	printk(KERN_INFO "\n");
}

void testResizingLL(void){
	int az;
	printk(KERN_INFO "*****testResizingLL()!\n");
	initCache();
	printk(KERN_INFO "Cache size is now: %d!\n", cache -> size);
	printk(KERN_INFO "Cache loadfactor is now: %d!\n", cache -> loadFactor);
	add("aaaaaaaaaaaaaaaaaaae", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("aaaaaaaaaaaaaaaaaabd", NULL);
	add("aaaaaaaaaaaaaaaaaacc", NULL);
	printCache();
	add("dddddddddddddddddddd", NULL);
	add("eeeeeeeeeeeeeeeeeeee", NULL);
	add("ffffffffffffffffffff", NULL);
	add("gggggggggggggggggggg", NULL);
	add("hhhhhhhhhhhhhhhhhhhh", NULL);
	add("iiiiiiiiiiiiiiiiiiii", NULL);
	add("jjjjjjjjjjjjjjjjjjjj", NULL);
	printCache();

	printk(KERN_INFO "Cache size is now: %d!\n", cache -> size);
	printk(KERN_INFO "Cache loadfactor is now: %d!\n", cache -> loadFactor);
	//Checking hash indices are correct
	az = 'a'+'z';
	printk(KERN_INFO "a+z is %d\n", az);
	printk(KERN_INFO "(a+z) modulo 11 is: %d\n", (az % 11));
	printk(KERN_INFO "(a+z) modulo 23 is: %d\n", (az % 23));
	printk(KERN_INFO "h modulo 23 is: %d\n", ('h' % 23));
	deleteCache();
	printk(KERN_INFO "\n");
}

void testRemovingLL(void){
	printk(KERN_INFO "*****testRemovingLL()!\n");
	initCache();
	add("aaaaaaaaaaaaaaaaaaae", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("aaaaaaaaaaaaaaaaaabd", NULL);
	add("aaaaaaaaaaaaaaaaaacc", NULL);
	add("eaaaaaaaaaaaaaaaaaaa", NULL);
	add("aeaaaaaaaaaaaaaaaaaa", NULL);
	add("aaeaaaaaaaaaaaaaaaaa", NULL);
	add("aaaeaaaaaaaaaaaaaaaa", NULL);
	printCache();

	printk(KERN_INFO "Removing head: b\n");
	delete("aaaaaaaaaaaaaaaaaaae", NULL);
	printCache();

	printk(KERN_INFO "Removing head's next: by\n");
	delete("aaaaaaaaaaaaaaaaaacc", NULL);
	printCache();

	printk(KERN_INFO "Removing middle: dw\n");
	delete("aaeaaaaaaaaaaaaaaaaa", NULL);
	printCache();

	printk(KERN_INFO "Removing end: fu\n");
	delete("aaaeaaaaaaaaaaaaaaaa", NULL);
	printCache();
	deleteCache();
	printk(KERN_INFO "\n");
}

void testGettingLL(void){
	CacheEntry* entry;
	printk(KERN_INFO "*****testGettingLL()!\n");
	initCache();
	add("aaaaaaaaaaaaaaaaaaae", NULL);
	add("bbbbbbbbbbbbbbbbbbbb", NULL);
	add("aaaaaaaaaaaaaaaaaabd", NULL);
	add("aaaaaaaaaaaaaaaaaacc", NULL);
	add("eaaaaaaaaaaaaaaaaaaa", NULL);
	add("aeaaaaaaaaaaaaaaaaaa", NULL);
	add("aaeaaaaaaaaaaaaaaaaa", NULL);
	add("aaaeaaaaaaaaaaaaaaaa", NULL);
	printCache();

	//Getting head;
	entry = get("aaaaaaaaaaaaaaaaaaae");
	printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);
	//Getting next
	entry = get("aaaaaaaaaaaaaaaaaabd");
	printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);

	//Getting middle
	entry = get("aeaaaaaaaaaaaaaaaaaa");
	printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);

	//Getting end
	entry = get("aaaeaaaaaaaaaaaaaaaa");
	printk(KERN_INFO "Entry's hash is: %s\n", entry -> hash);

	//Getting does not exist
	entry = get("gt");
	if(entry == NULL){
		printk(KERN_INFO "Entry is NULL\n");
	}
	deleteCache();
	printk(KERN_INFO "\n");
}

// int main(){
// 	//testAdding();
// 	//testResizing();
// 	// testRemoving();
// 	// testGetting();
// 	//testAddingLL();
// 	//testResizingLL();
// 	// testRemovingLL();
// 	testGettingLL();

// 	return 0;
// }